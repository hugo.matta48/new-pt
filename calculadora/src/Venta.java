public class Venta {
    private Producto producto;
    private Integer cantidad;
    private Float montoTotal;
    private Float IGV;

    public Float getMontoTotal(float v) {
        return montoTotal;
    }

    public void setMontoTotal(Float montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Float getIGV() {
        return IGV;
    }

    public void setIGV(Float IGV) {
        this.IGV = IGV;
    }
}
