public class Transaccion {
    public static void main(String[] args) {
        Producto producto = new Producto();
        producto.setColor("verde");
        producto.setMarca("Pilot");
        producto.setModelo("Board Master");
        producto.setPrecio(10.50f);
        producto.setStock(150);

        Venta venta = new Venta();
        venta.setCantidad(10);
        venta.setProducto(producto);
        venta.setIGV(0.18f
                *venta.getProducto().getPrecio()
                *venta.getCantidad();
        );
        venta.getMontoTotal(
          venta.getIGV()
                +venta.getProducto().getPrecio()
                *venta.getCantidad()
        );
    }
}
