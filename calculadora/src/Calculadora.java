public class Calculadora {
    private Double primero;
    private Double segundo;
    private Double resultado;

    public Double sumar() {
        return primero + segundo;
    }
    public Double restar() {
        return primero - segundo;
    }
    public Double multiplicar() {
        return primero * segundo;
    }
    public Double dividir() {
        return primero / segundo;
    }


    public static void main(String[] args) {
        Calculadora a = new Calculadora();
        a.primero = 5.0;
        a.segundo = 5.8;
        a.sumar();
        System.out.println(a.resultado);
        a.resultado = a.restar();
        System.out.println(a.resultado);
        a.resultado = a.multiplicar();
        System.out.println(a.resultado);
        a.resultado = a.dividir();
        System.out.println(a.resultado);
    }

}
